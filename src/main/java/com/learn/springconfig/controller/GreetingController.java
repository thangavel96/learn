package com.learn.springconfig.controller;

import com.learn.springconfig.config.DbSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class GreetingController {


    @Value("${my.greeting}")
    private String greetingMessage;

    @Autowired
    private DbSetting dbSetting;

    @GetMapping(path = "/greeting")
    public String greetingMessage(){
        String dbInfo = "DB Details from config server: Host: "+dbSetting.getConnection();
        return "Greeting!! "+ greetingMessage+ " "+dbInfo;
    }

}
