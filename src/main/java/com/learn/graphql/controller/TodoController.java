package com.learn.graphql.controller;

import com.learn.graphql.dto.TodoDto;
import com.learn.graphql.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class TodoController {

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @Autowired
    TodoService todoService;
    @QueryMapping
    public List<TodoDto> getAllTodo() {
        System.out.println("hit");
        return todoService.getAllTodos();
    }

    @QueryMapping
    public TodoDto getTodoById(@Argument String id){
        return todoService.getTodoById(id);
    }

    @MutationMapping
    public TodoDto createTodo(@Argument(value = "todo")TodoDto todoDto){
        return todoService.insertTodo(todoDto);
    }
}
