package com.learn.graphql.dao;

import com.learn.graphql.dto.TodoDto;

import java.util.List;

public interface TodoDao {

    public List<TodoDto> getAllTodoList();

    public TodoDto getTodoById(String id);

    TodoDto insertTodo(TodoDto todoDto);
}
