package com.learn.graphql.service;

import com.learn.graphql.dto.TodoDto;
import com.learn.graphql.dao.TodoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;

@Component
public class TodoService {

    @Autowired
    public TodoDao todoDao;

    public List<TodoDto> getAllTodos() {
        return todoDao.getAllTodoList();
    }

    public TodoDto getTodoById(String id){
        return todoDao.getTodoById(id);
    }

    public TodoDto insertTodo(TodoDto todoDto) {
        //todo check if the todo is already exists, if yes throw exception
        todoDto.setId(getNewId());
        return todoDao.insertTodo(todoDto);
    }

    public String getNewId() {
        int nextValue = todoDao.getAllTodoList()
                .stream()
                .sorted(Comparator.comparingInt(t -> Integer.parseInt(t.getId())))
                .reduce((first, second) -> second)
                .map(todoDto -> Integer.valueOf(todoDto.getId()) + 1)
                .orElse(0);
        return String.valueOf(nextValue);
    }
}
