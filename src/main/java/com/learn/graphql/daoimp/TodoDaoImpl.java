package com.learn.graphql.daoimp;

import com.learn.graphql.dto.TodoDto;
import com.learn.graphql.dao.TodoDao;
import com.learn.graphql.dto.TodoItem;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class TodoDaoImpl implements TodoDao {

    List<TodoDto> todoDtoList;
    public TodoDaoImpl() {
        this.todoDtoList = new ArrayList<>(Arrays.asList(
                TodoDto.builder()
                        .id("0")
                        .name("Details for weekend purchase")
                        .title("Weekend")
                        .markAsComplete(false)
                        .items(new ArrayList<>(Arrays.asList(TodoItem.builder()
                                        .markAsComplete(false)
                                        .notes("New Items needs to be added")
                                        .id("0")
                                .build())))
                        .build(),
                TodoDto.builder()
                        .id("1")
                        .name("Details for weekend purchase")
                        .title("Weekend")
                        .markAsComplete(false)
                        .items(new ArrayList<>(Arrays.asList(TodoItem.builder()
                                .markAsComplete(false)
                                .notes("New Items needs to be added")
                                .id("0")
                                .build())))
                        .build()
        ));
    }

    @Override
    public List<TodoDto> getAllTodoList() {
        return this.todoDtoList;
    }

    @Override
    public TodoDto getTodoById(String id) {
        return this.todoDtoList.stream().filter(todoDto -> id.equals(todoDto.getId())).findAny().orElse(null);
    }

    @Override
    public TodoDto insertTodo(TodoDto todoDto) {
        this.todoDtoList.add(todoDto);
        return todoDto;
    }
}
