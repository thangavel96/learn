package com.learn.graphql.scalar;

import graphql.schema.Coercing;
import graphql.schema.GraphQLScalarType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Configuration
public class GraphQLConfig {


    @Bean
    public GraphQLScalarType localDateScalar() {
        return GraphQLScalarType.newScalar()
                .name("LocalDate")
                .description("Local date scalar")
                .coercing(new Coercing<LocalDate, String>() {
                    @Override
                    public String serialize(Object dataFetcherResult) {
                        return DateTimeFormatter.ISO_LOCAL_DATE.format((LocalDate) dataFetcherResult);
                    }

                    @Override
                    public LocalDate parseValue(Object input) {
                        return LocalDate.parse((String) input, DateTimeFormatter.ISO_LOCAL_DATE);
                    }

                    @Override
                    public LocalDate parseLiteral(Object input) {
                        return LocalDate.parse((String) input, DateTimeFormatter.ISO_LOCAL_DATE);
                    }
                })
                .build();
    }


}
