package com.learn.graphql.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class TodoDto {

    private String id;
    private String title;
    private String name;
    private boolean markAsComplete;
    private List<TodoItem> items;
}
