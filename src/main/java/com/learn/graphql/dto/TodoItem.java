package com.learn.graphql.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TodoItem {

    private String id;
    private String notes;
    private boolean markAsComplete;


}
